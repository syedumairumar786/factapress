<div id="off-canvas" class="offcanvas-full-screen" data-off-canvas data-transition="overlap">
  <div class="offcanvas-full-screen-inner">

  <div class="grid-x grid-margin-x">
    <div class="medium-12 cell">
      <?php joints_off_canvas_nav(); ?>

      <?php if ( is_active_sidebar( 'offcanvas' ) ) : ?>

        <?php dynamic_sidebar( 'offcanvas' ); ?>

      <?php endif; ?>
    </div>
  </div>

  </div>
</div>
