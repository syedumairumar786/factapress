<?php


/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('hero'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">

		<div class="medium-12 cell">
			<section class="entry-content" itemprop="text">
			<?php // the_post_thumbnail('full'); ?>
			<?php the_content(); ?>
		</section> <!-- end article section -->


		</div>
	</div>




	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</div>


</article> <!-- end article -->
