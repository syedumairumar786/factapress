<svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 52 (66869) - http://www.bohemiancoding.com/sketch -->
    <title>linkedin icon</title>
    <desc>Created with Sketch.</desc>
    <defs><style>#Fill-1 {fill: #FFFFFF}</style></defs>
    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="-footer-3" transform="translate(-1135.000000, -224.000000)" fill="#4D4D4D">
            <g id="linkedin-icon" transform="translate(1135.000000, 224.000000)">
                <polygon id="Fill-1" points="0.2372 11.6368 0.2372 21.9768 5.1742 21.9768 5.1742 10.8048 5.1742 7.9008 0.2372 7.9008"></polygon>
                <g id="Group-8" transform="translate(0.000000, 0.651400)">
                    <path d="M2.706,0.211 C1.343,0.211 0.237,1.314 0.237,2.675 C0.237,4.035 1.343,5.138 2.706,5.138 C4.069,5.138 5.174,4.035 5.174,2.675 C5.174,1.314 4.069,0.211 2.706,0.211" id="Fill-2"></path>
                    <path d="M21.2831,11.5714 C20.9491,8.9184 19.7281,7.2494 16.1351,7.2494 C14.0031,7.2494 12.5721,8.0404 11.9871,9.1484 L11.9261,9.1484 L11.9261,7.2494 L7.9941,7.2494 L7.9941,9.9504 L7.9941,21.3264 L12.1041,21.3264 L12.1041,14.3484 C12.1041,12.5084 12.4521,10.7274 14.7321,10.7274 C16.9791,10.7274 17.1591,12.8314 17.1591,14.4674 L17.1591,21.3264 L21.3901,21.3264 L21.3901,13.5914 L21.3911,13.5914 C21.3911,12.8694 21.3611,12.1934 21.2831,11.5714" id="Fill-4"></path>
                </g>
            </g>
        </g>
    </g>
</svg>
