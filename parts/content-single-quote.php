<h3 class="quote" <?php if (get_field('quote_lineheight')): ?>style="line-height: <?php the_field('quote_lineheight'); ?>;"<?php endif; ?>>
<?php the_field('author_quote'); ?>
</h3>
