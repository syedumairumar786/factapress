<section class="share-profile">
  <h3 class="section-title">Share Profile</h3>
  <div class="social-nav social-tpn">
  <a href="#" target="blank"><?php echo get_template_part('parts/svgs/facebook-tpn'); ?></a>
  <a href="#" target="blank"><?php echo get_template_part('parts/svgs/twitter-tpn'); ?></a>
  <a href="#" target="blank"><?php echo get_template_part('parts/svgs/linkedin-share'); ?></a>
  <a href="#" target="blank"><?php echo get_template_part('parts/svgs/medium'); ?></a>
  <a href="#" target="blank"><?php echo get_template_part('parts/svgs/mail'); ?></a>

        </div>
</section>
