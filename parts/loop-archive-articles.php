<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('type-read'); ?> role="article">

	<header class="article-header">
		<div class="grid-x">
			<div class="medium-8 cell">
				<?php
				$tags = get_the_tags();

				if ($tags) {
					?>
					<ul class="tags menu horizontal">
						<?php
						foreach($tags as $tag) {
							$tag_url = site_url() . '/' . 'tag/' . $tag->slug;
				        echo "<li><a href='$tag_url'>$tag->name</a></li>";
				   }
					 ?>
					</ul>
					<?php
				}

		     ?>
			</div>
			<?php if (function_exists('get_field')): ?>
			<div class="medium-4 cell">
				<?php
				$terms = get_the_terms( get_the_ID(), 'article_source' );

	if ( $terms && ! is_wp_error( $terms ) ) :

	$source_links = array();

	foreach ( $terms as $term ) {
	$source_links[] = $term->name;
	}

	$on_source = join( ", ", $source_links );
	?>

	<?php if ($on_source == 'The Progress Network'): ?>
			<p class="subline float-right"><a href="<?php echo site_url(); ?>/article_source/the-progress-network"><?php printf( esc_html__( '%s', 'jointswp' ), esc_html( $on_source ) ); ?></a></p>
	<?php else: ?>
		<?php if (get_field('aggregated_content_link')): ?>
		<p class="subline external float-right"><a href="<?php the_field('aggregated_content_link'); ?>" target="_blank"><?php printf( esc_html__( '%s', 'jointswp' ), esc_html( $on_source ) ); ?></a></p>
<?php else: ?>
<p class="subline external float-right"><?php printf( esc_html__( '%s', 'jointswp' ), esc_html( $on_source ) ); ?></p>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
			</div>
		<?php endif; ?>
		</div>

		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="text">

		<?php //the_content('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>');
		      // the_excerpt();
					if (function_exists('get_field')):
						?>
						<h3 class="subtitle"><?php the_field('subtitle'); ?></h3>
						<?php
						if (get_field('excerpt')):
					the_field('excerpt');
				else:
					the_excerpt();
				endif;
				else:
					the_excerpt();
				endif;
		 ?>
	</section> <!-- end article section -->

<?php if (function_exists('get_field')): ?>

	<?php if (get_field('show_more_content')):

$number = rand();
		 ?>
	<div class="more-content-wrap">
		<div class="text-center">
			<button data-toggle="content-<?php echo $number; ?>" target="_blank" class="showmorebtn button primary">Show More</button>
		</div>
<div id="content-<?php echo $number; ?>" class="showmore hide" data-toggler=".hide">
	<?php the_field('show_more_content'); ?>
</div>
	</div>
	<?php endif; ?>
	<div class="text-center more-story">

	<?php if (!get_field('aggregated_content_link')): ?>
		<a href="<?php the_permalink(); ?>" target="_blank" class="button primary">
			<?php _e('More on this story', 'jointswp'); ?>
		</a>
	<?php endif; ?>

	</div>
<?php endif; ?>
<!--<footer class="article-footer">
	<?php// get_template_part( 'parts/content', 'byline-tpn-guest' ); ?>

</footer>--> <!-- end article footer -->
<?php get_template_part( 'parts/content', 'byline-tpn' ); ?>

</article> <!-- end article -->
