<?php
/**
 * The template part for displaying a grid of posts
 *
 * For more info: http://jointswp.com/docs/grid-archive/
 */

?>

		<!--Item: -->
		<div class="medium-12 large-4 cell panel" data-equalizer-watch>

			<article id="post-<?php the_ID(); ?>"  <?php post_class('type-read'); ?> role="article">

        <header class="article-header">
      		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
      		<?php get_template_part( 'parts/content', 'byline-tpn' ); ?>
      	</header> <!-- end article header -->

        <div class="grid-x">
          <div class="medium-12 cell">
						<?php
						$terms = get_the_terms( get_the_ID(), 'article_source' );

			if ( $terms && ! is_wp_error( $terms ) ) :

			$source_links = array();

			foreach ( $terms as $term ) {
			$source_links[] = $term->name;
			}

			$on_source = join( ", ", $source_links );
			?>

			<?php if ($on_source == 'The Progress Network'): ?>
					<p class="subline"><a href="<?php echo site_url(); ?>/article_source/the-progress-network"><?php printf( esc_html__( '%s', 'jointswp' ), esc_html( $on_source ) ); ?></a></p>
			<?php else: ?>
				<?php if (get_field('aggregated_content_link')): ?>
				<p class="subline external"><a href="<?php the_field('aggregated_content_link'); ?>" target="_blank"><?php printf( esc_html__( '%s', 'jointswp' ), esc_html( $on_source ) ); ?></a></p>
			<?php else: ?>
			<p class="subline external"><?php printf( esc_html__( '%s', 'jointswp' ), esc_html( $on_source ) ); ?></p>
			<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
          </div>
        </div>



      <ul class="tags menu horizontal"><?php
      $tags = get_the_tags();

      foreach($tags as $tag) {
          echo "<li>$tag->name</li>";
     } ?></ul>

      	<footer class="article-footer">

      	</footer> <!-- end article footer -->

			</article> <!-- end article -->

		</div>
