<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('hero'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">


	<div class="grid-x grid-margin-x grid-padding-x align-middle">
		<div class="medium-12 cell">

<div class="grid-x">
	<div class="medium-12 cell">

		<header class="article-header text-center">

			<h1 class="entry-title single-title" itemprop="headline">
<?php the_title(); ?>
			</h1>
			<?php // get_template_part( 'parts/content', 'byline' ); ?>
			<?php if (function_exists('get_field')): ?>
				<?php if (get_field('subtitle')): ?>
				<h3 class="subtitle"><?php the_field('subtitle'); ?></h3>
			<?php endif; ?>
		<?php endif; ?>


			</header> <!-- end article header -->

		<section class="entry-content" itemprop="text">
		<div class="grid-container">
			<div class="post-meta">
			<div class="grid-x grid-margin-x">
				<?php

				if ( shortcode_exists( 'rt_reading_time' ) ) {
					 ?>
					 <div class="small-6 cell">
						<?php  echo do_shortcode('[rt_reading_time label="Read Time:" postfix="minutes"]'); ?>
					 </div>
					 <?php
				}

					?>
					<div class="small-6 cell">
						<span ><?php the_date('F j, Y'); ?></span>
					</div>
			</div>
			</div>
	<div class="post-content">
			<?php the_content(); ?>
	</div>
		</div>

	</section> <!-- end article section -->

	<footer class="article-footer">

	</footer>

	</div>
</div>

		</div>

	</div>




	<footer class="article-footer">
		<div class="grid-container">
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
			<ul class="tags menu horizontal"><?php
			$tags = get_the_tags();

			foreach($tags as $tag) {
					echo "<li>$tag->name</li>";
		 } ?></ul>
		</div>
	</footer> <!-- end article footer -->

	<div class="grid-container">
		<div class="grid-x grid-padding-x grid-margin-x">
			<?php comments_template(); ?>
		</div>
	</div>




</article> <!-- end article -->
