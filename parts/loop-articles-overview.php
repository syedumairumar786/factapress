<section class="articles">
  <?php

  $args = array(
      'post_type' => 'post',
      'posts_per_page' => -1
  );

// The Query
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {

    ?>
<div class="grid-container">
  <?php
  while ( $the_query->have_posts() ) {
      $the_query->the_post();

      get_template_part('parts/loop', 'archive-articles');


  }
  ?>
</div>
    <?php

} else {
    // no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
?>

<div class="text-center">
  <a href="#" class="button secondary more-articles">Want to see more articles?</a>
</div>

</section>
