<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="top-bar" id="top-bar-menu">
	<div class="top-bar-left float-left">
		<ul class="menu">
			<li><a class="site-logo" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></li>
		</ul>
	</div>
	<div class="top-bar-right float-right header-bottom">
		<ul class="menu align-middle">
			<li></li>
			<li>
				<a href="#">
<span class="cta"><?php _e('Hello, I am a CTA', 'jointswp'); ?></span>
			</a></li>
			<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
			<li>
				<button class="hamburger hamburger--collapse" type="button" data-toggle="off-canvas">
				  <span class="hamburger-box">
				    <span class="hamburger-inner"></span>
				  </span>
				</button>
			</li>
			<!--<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>-->
		</ul>
	</div>
</div>
