<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('hero'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">


<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x align-middle">
		<div class="medium-12 cell">
			<h1 class="section-title" ><?php _e('Cool stuff <span>to look at</span>', 'jointswp'); ?></h1>

			<?php

		  $args = array(
		      'post_type' => 'post',
		      'posts_per_page' => -1
		  );

		// The Query
		$the_query = new WP_Query( $args );

		// The Loop
		if ( $the_query->have_posts() ) {

		    ?>
		<div class="articles-slider">
		  <?php
		  while ( $the_query->have_posts() ) {
		      $the_query->the_post();
		      ?>
					<div class="article-slide">
						<div class="grid-x grid-padding-x grid-margin-x">
							<div class="medium-12 large-7 cell">
								<?php if (has_post_thumbnail()): ?>
								<div class="author-img">
								<?php the_post_thumbnail(); ?>
								</div>
							<?php endif; ?>

							</div>
							<div class="medium-12 large-5 cell">

								<header class="article-header">

									<h1 class="entry-title single-title" itemprop="headline">
					<a href="<?php if (get_field('aggregated_content_link')): the_field('aggregated_content_link'); else: the_permalink(); endif; ?>" <?php if (get_field('aggregated_content_link')): echo 'target="_blank"'; endif; ?>><?php the_title(); ?></a>
									</h1>
									<?php // get_template_part( 'parts/content', 'byline' ); ?>
									</header> <!-- end article header -->
								<section class="entry-content" itemprop="text">
								<?php // the_post_thumbnail('full'); ?>
								<?php // the_content(); ?>

							</section> <!-- end article section -->


							</div>
						</div>

						<div class="grid-x grid-padding-x grid-margin-x">
							<div class="medium-12 large-7 cell">
								<div class="dots-wrap">

								</div>
							</div>
						</div>

					</div>
					<?php


		  }
		  ?>
		</div>
		    <?php

		} else {
		    // no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
		?>

		</div>

	</div>




	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</div>


</article> <!-- end article -->
