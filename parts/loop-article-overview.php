<?php

$headshot = get_field('large_headshot');

/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	<div class="hero-bg">
	</div>
	<div class="top-left-divider">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 513.61 685.49" preserveAspectRatio="none"><defs><style>.cls-1{fill:#3b6ee0;}</style></defs><title>divider-top-left</title><g id="Layer_2" data-name="Layer 2"><g id="COOL_STUFF_copy_2" data-name="COOL STUFF copy 2"><path class="cls-1" d="M509.31,435.63a215.4,215.4,0,0,1-8.42,31.89c-12.38,35.3-32.84,60.8-58.51,79.42-69.54,50.44-177.31,50.37-266.3,57.78-72,6-129.29,31.79-176.08,80.77V0H299.63c.8,41,4.28,87.27,17.47,111.36,16.62,30.39,46.47,50.94,74.95,69.17,56.68,36.3,116,70.34,119.54,145.38C513.29,362.18,516,399.7,509.31,435.63Z"/></g></g></svg>
	</div>
	<div class="top-right-divider">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 737.58 524.66" preserveAspectRatio="none"><defs><style>.cls-1{fill:#ffe6a1;}</style></defs><title>divider-top-right</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g id="Layer_2-2" data-name="Layer 2"><g id="COOL_STUFF_copy_2" data-name="COOL STUFF copy 2"><path class="cls-1" d="M736.75,293.06H737V467.92C699.9,499,647.52,511.31,601.13,518.8a457.91,457.91,0,0,1-162.71-3c-44-8.84-74.94-30.88-122.29-41.39.88-2.24,1.73-4.52,2.53-6.85H261.72c-32.39-4.12-64.08-13.33-96.64-15-17.85-.92-35.48-1.4-52.48-3.76-22.71-3.13-44.41-9.58-64.35-24.82-20.78-15.9-42-42.07-46-68.3-5.67-36.9.44-67.72,6.49-103.9,2.34-14,5.26-28.87,16.45-38.93,9.67-8.72,2.65-199,16.2-201.85C59.16,7.27,80.14,3.56,103.58,0h634Z"/></g></g></g></g></svg>
	</div>

<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x align-middle">
		<div class="medium-12 cell">
			<h1 class="section-title" ><?php _e('Cool stuff <span>to look at</span>', 'jointswp'); ?></h1>
			<?php

$posts = get_field('article_slider');

if( $posts ): ?>
    <div class="articles-slider">
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	        <?php setup_postdata($post);

					$image = get_field('slider_image');


					 ?>
	        <div class="article-slide">
						<div class="grid-x grid-padding-x grid-margin-x">
							<div class="medium-12 large-7 cell">
								<?php if (!empty($image)): ?>
								<div class="author-img">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
								</div>
							<?php endif; ?>

							</div>
							<div class="medium-12 large-5 cell">

								<header class="article-header">
									<p class="subline">The Progress Network</p>
									<h1 class="entry-title single-title" itemprop="headline">
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h1>
									<?php // get_template_part( 'parts/content', 'byline' ); ?>
									<p class="author-title <?php if (get_field('official_network_member')): echo 'has-badge'; endif; ?>"><?php the_field('title'); ?></p>
									</header> <!-- end article header -->
								<section class="entry-content" itemprop="text">
								<?php // the_post_thumbnail('full'); ?>
								<?php // the_content(); ?>

							</section> <!-- end article section -->

							<footer class="article-footer">
									<?php

$posts = get_field('author');

if( $posts ): ?>
    <div class="grid-x align-middle">
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	        <?php setup_postdata($post); ?>
	       <?php if (has_post_thumbnail()): ?>
					 <div class="medium-2 cell">
						 <div class="post-thumb">
						 	<?php the_post_thumbnail(); ?>
						 </div>
		       </div>
				 <?php endif; ?>
				 <div class="medium-10 cell">
					 <h4 class="author-name">By<br>
					 <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				 </div>
	    <?php endforeach; ?>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
							</footer>

							</div>
						</div>

						<div class="grid-x grid-padding-x grid-margin-x">
							<div class="medium-12 large-7 cell">
								<div class="dots-wrap">

								</div>
							</div>
						</div>

	        </div>
	    <?php endforeach; ?>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

		</div>

	</div>




	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</div>

<?php

if (get_field('author_quote')):

 get_template_part('parts/content', 'single-quote');

endif;

  ?>


</article> <!-- end article -->
