<?php
/**
 * Template Name: Article Overview
 */

get_header('style-three'); ?>

<div class="content">

	<div class="inner-content grid-x grid-margin-x grid-padding-x">

		<main class="main small-12 cell" role="main">

		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		    	<?php // get_template_part( 'parts/loop', 'article-slider' ); ?>

					<?php get_template_part( 'parts/loop', 'articles-overview' ); ?>

					<?php get_template_part('parts/content', 'newsletter'); ?>

		    <?php endwhile; else : ?>

		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

		<?php // get_sidebar(); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
