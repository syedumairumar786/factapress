<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>


				<footer class="footer" role="contentinfo">

					<div class="inner-footer grid-x grid-margin-x grid-padding-x">

            <div class="small-12 cell">
              <?php if (function_exists('get_field')): ?>
                <div class="social-nav">
                  <?php if (get_field('global_twitter_link', 'option')): ?>
                  <a href="<?php the_field('global_twitter_link', 'option'); ?>" target="blank"><?php echo get_template_part('parts/svgs/twitter'); ?></a>
                <?php endif; if (get_field('global_facebook_link', 'option')): ?>
                        <a href="<?php the_field('global_facebook_link', 'option'); ?>" target="blank"><?php echo get_template_part('parts/svgs/facebook'); ?></a>
                      <?php endif; if (get_field('global_youtube_link', 'option')): ?>
                    <a href="<?php the_field('global_youtube_link', 'option'); ?>" target="blank"><?php echo get_template_part('parts/svgs/youtube'); ?></a>
                  <?php endif; if (get_field('global_instagram_link', 'option')): ?>
                    <a href="<?php the_field('global_instagram_link', 'option'); ?>" target="blank"><?php echo get_template_part('parts/svgs/instagram'); ?></a>
                  <?php endif; ?>

                      </div>
                    <?php endif; ?>

            </div>

						<div class="small-12 cell">
							<nav role="navigation">
	    						<?php joints_footer_links(); ?>
	    					</nav>
                <a href="#" style="color: #fff; margin-top: -15px; margin-bottom: 15px; display: block;">Privacy Policy</a>
	    				</div>

              <div class="small-12 cell">
                <div class="footer-sub">
                  <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
                </div>
              </div>

					</div> <!-- end #inner-footer -->

				</footer> <!-- end .footer -->

			</div>  <!-- end .off-canvas-content -->

		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
