jQuery(document).ready(function( $ ) {

	// $ Works! You can test it with next line if you like
	// console.log($);

  $.fn.wordify = function(){
  this.find(":not(iframe,textarea)").addBack().contents().filter(function() {
    return this.nodeType === 3;
  }).each(function() {
    var textnode = $(this);
    var text = textnode.text();
    text = text.replace(/([^\s-.,;:!?()[\]{}<>"]+)/g,'<span>$1</span>');
    textnode.replaceWith(text);
  });
  return this;
};


  var $hamburger = $(".hamburger");
    $('#off-canvas').on('opened.zf.offcanvas', function(event) {
      //$('.pagination').css('display','block');
    //  console.log("Off-canvas menu was opened.");
      $hamburger.addClass("is-active");

    });

  $('#off-canvas').on('closed.zf.offcanvas', function(event) {
    //$('.pagination').css('display','block');
  //  console.log("Off-canvas menu was closed.");
    $hamburger.removeClass("is-active");

  });

  // Articles Slider
$('body:not(.non-slider) .articles-slider').slick({
  dots: true,
  arrows: false,
  appendDots: $('.dots-wrap')
});



});
